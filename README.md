# CIS Back Office

## การติดตั้ง

- เข้าไปตั้งค่า Database ที่ไฟล์ `wp-config.php` แล้วเปลี่ยน  
  `define( 'DB_NAME', '{Database_Name}');`  
  `define( 'DB_USER', '{Database_Username}');`  
  `define( 'DB_PASSWORD', '{Database_Password}');`  
  `define( 'DB_HOST', '{Database_Host}');`

## การเรียกใช้ API

- บทความ  
  **GET** `/wp-json/wp/v2/posts`
- รายชื่อศิญย์เก่า  
  **GET** `/wp-json/wp/v2/wp-json/wp/v2/alumnies`
